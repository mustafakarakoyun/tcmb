package com.example.tcbm_example.service;

import com.example.tcbm_example.entities.Currency;

import java.util.List;

public interface TcmbServiceImpl {

 void   save (Currency currency) ;
 List<Currency> listAll();
}
