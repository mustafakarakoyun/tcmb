package com.example.tcbm_example.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.example.tcbm_example.entities.Currency;
import com.example.tcbm_example.response.HttpResponseMessage;
import com.example.tcbm_example.service.TcmbServiceImpl;
import com.example.tcbm_example.service.impl.TcmbService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/currency")

public class TcmbRestController {
    @Autowired
    TcmbService tcmb;

    private final TcmbService tcmbService;
    private static final Logger logger = LoggerFactory.getLogger(TcmbServiceImpl.class);

    @GetMapping("/get_currency_tcmb")
    public Map<String, Object> currencies() {
        Map<String, Object> hm = new LinkedHashMap<>();
        hm.put("currencies", tcmb.todayXml() );
        return hm;
    }
    @Cacheable(cacheNames = "currencies")
    @GetMapping(value = {"/list_currencies_from_db"})
    public ResponseEntity<?> getAll(){
        List<Currency> currencies = tcmbService.listAll();

        HttpResponseMessage message = new HttpResponseMessage.HttpResponseMessageBuilder()
                .success(true)
                .items(currencies)
                .build();
        logger.info("Brings the books");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
