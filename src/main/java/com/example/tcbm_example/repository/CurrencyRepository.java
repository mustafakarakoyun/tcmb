package com.example.tcbm_example.repository;

import com.example.tcbm_example.entities.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository  extends JpaRepository <Currency, Integer>{
}
