package com.example.tcbm_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class TcbmExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TcbmExampleApplication.class, args);
    }

}
